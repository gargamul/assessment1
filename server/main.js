/**
 * Created by Amulya on 21/10/2016.
 */
//loading my packages
var express=require('express');
var bodyParser = require('body-parser');

var app=express();
var PORT=process.env.NODE_PORT || 5000;

app.use(bodyParser.json());
app.use(express.static(__dirname+'/../client'));

app.post('/api/submit', function(req,res){
    console.log("My request body: "+JSON.stringify(req.body));
    res.json(req.body);

});

app.use('/',function(req,res){
    res.redirect('Registration.html');

});

app.listen(PORT, function(){
    console.log("Server listening at port: "+PORT);
});