/**
 * Created by Amulya on 21/10/2016.
 */
(function(){

    var MyApp=angular.module('MyApp',[]);

    var myCtrl=function($http){

        var vm=this;
        vm.newUser={
            email:"",
            password:"",
            name:"",
            gender:"Female",
            dob:"",
            address:"",
            country:"",
            contact:""
        };

        vm.isAgeValid = function () {
            var date = new Date(vm.newUser.dob);
            date.setFullYear(date.getFullYear() + 18);
            console.log(date<new Date());
            console.log('the date is:' + date);
            return date < new Date();
        };

        vm.register = function () {
            if (vm.isAgeValid() == false) {
                return;
            }
            $http.post("/api/submit",vm.newUser)
                .then(function () {
                    window.location = "/thank-you.html"
                })
                .catch(function () {
                    alert("Oops! Some problem occurred after registration. Please try again.")
                });
        };


    };//close of controller


    MyApp.controller('myCtrl',["$http",myCtrl]);

})();
